/*
 * $Id$
 * Copyright (c) 2005 IRIT-UPS
 *
 * include/util/Dominance.h -- Dominance class interface.
 */
#ifndef OTAWA_UTIL_DOMINANCE_H
#define OTAWA_UTIL_DOMINANCE_H

#include <otawa/cfg/Dominance.h>
#warning "<otawa/util/Dominance.h> deprecated: use <otawa/cfg/Dominance.h> instead."

#endif // OTAWA_UTIL_DOMINANCE_H
